---

## Lift Up State

In this scenario, due to Form & PackingList are sibing component, we cannot use props to pass the data over.


![alt text](image-34.png)

![alt text](image-35.png)

```jsx
function Form() {
  const [description, setDescription] = useState("");
  const [quantity, setQuantity] = useState(1);
  const [item, setItem] = useState([]); //Items Array, passed by **newItem

  function handleAddItems(item) {
    setItem((items) => [...items, item]);
  }
------------------------------------------------------------------------------
function PackingList() {
  return (
    <div className="list">
      <ul>
        {initialItems.map((item) => (
          <Item propped={item} key={item.id} />
        ))}
      </ul>
    </div>
  );
}
```

---

In this case, we simply move the component to the **COMMON PARENT** component. Which is the APP component. (See Img #2)

```jsx title="State Pass Down"
export default function App() {
  const [items, setItems] = useState([]);

  function handleAddItems(item) {
    setItems((items) => [...items, item]);
  }

  return (
    <div className="app">
      <Logo />
      <Form onAddItems={handleAddItems} />
      <PackingList items={items} />
      <Stats />
    </div>
  );
}
```

Now, the State is in **APP** function, and as a parent component, it pass down the state as props.



```jsx title="It is the form that is actually responsible for creating new items. Therefore, we need to give this component (FORM) access to a function that can update the state. />"
  function Form({ onAddItems }) {
    const [description, setDescription] = useState("");
    const [quantity, setQuantity] = useState(1);

    function handleSubmit(e) {
    e.preventDefault();

    if (!description) return;

    const newItem = { description, quantity, packed: false, id: Date.now() };
    console.log(newItem);

    onAddItems(newItem);

    setDescription("");
    setQuantity(1);
  }
```


```jsx title="Displaying the items array in the PackingList"
function PackingList({ items }) {
  return (
    <div className="list">
      <ul>
        {items.map((item) => (
          <Item propped={item} key={item.id} />
        ))}
      </ul>
    </div>
  );
}

```
!!! Result
    ![alt text](image-36.png)

!!! Note

    Whenever multiple sibling components need access to the same state, we move that piece of state up to the first common parent component, which again, in our case here, was the APP component.