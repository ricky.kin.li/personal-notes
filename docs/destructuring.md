---

##==Destruturing==

### Basic Destructuring

```js title="Example 1"
const game = {
    name: "Breakout",
    year: 1990,
    developer: "Shit Games"
};

// TODO: In one single assignment, obtain a reference to the game's developer name.

const {name, year, developer} = game;

console.log(`Developer: ${developer}`);
// Correct result should be:
// Developer: Shit Games
```


```js title="Example 2"
const notebooks = [
    {
        brand: "Asus",
        price: 499
    },
    {
        brand: "Lenovo",
        price: 699
    },
    {
        brand: "Welcome",
        price: 1029
    }
];

// TODO: In one single assignment, obtain a reference to the third notebook's price.

const [notebook1, notebook2, {brand, price}] = notebooks;

console.log(`Price: ${price}`);
// Correct result should be:
// Price: 1029
```

### Nested Destructuring

```js
const book = {
  author: { firstName: "Ricky", lastName: "Li", city: "Hong Kong" },
  coAuthors: [
    {
      name: "Sam",
      contribution: 0.05,
    },
    {
      name: "Susan",
      contribution: 0.2,
    },
    {
      name: "Pauline",
      contribution: 0.1,
    },
  ],
  year: 1985,
  purchaseRecord: [
    {
      purchaser: "Jessie",
      price: 54,
      note: "",
    },
    {
      purchaser: "Stephen",
      price: 54,
      note: "",
    },
    {
      purchaser: "Jessica",
      price: 54,
      note: "",
    },
    {
      purchaser: "Murphy",
      price: 30,
      note: "SUMMER_SALES",
    },
  ],
};

// TODO: In one single assignment, obtain a reference to the author's first name (Ricky), publication year(1985), and the second purchaser's price(54)

const {
  author: { firstName },
  year,
  purchaseRecord: [firstPurchaser, { purchaser, price, note }],
} = book;
```