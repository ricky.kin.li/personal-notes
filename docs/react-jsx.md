---
##==JSX==

-   JSX is a **Declarative** syntax to describe what the components look like and how they work
-   Components must **return** a block of JSX
-   While it looks like **HTML**, but it actually is an extension of Javascript that allow us to embed **Javascript**, **CSS**, and **React** components into **HTML**.

!!! Example
    ![](image-7.png)

##==Converting JSX by BABEL ==(Included in create-react-app)==

!!! Example
    ![](image-8.png)

-   Browser does not understand JSX, they only understand **HTML**.
-   All JSX are converted into many nested ```React.createElement``` function call.

##==Rules of JSX==

---
###General JSX Rules

1.  JSX works  essentially like ```HTML```, but we can enter "JavasScript mode" by using ```{}``` (for text or attributes)
2.  We can place **JavaScript expressions** inside {}.
        
        Examples: reference variables, create array or objects, [].map(), ternary operator

3.  Statements are not allowed (if/else, for, switch)
4.  JSX produces a JavaScript expression
```js
const el = <h1>Hello React!</h1>;
const el = React.createElement("h1", null, "Hello React!");
```
5.  We can place other pieces of JSX inside {}
6.  We can write JSX anywhere inside a component (in if/else, assign to variables, pass it into functions)
7.  A piece of JSX can only have one root element. If you need more, use ```<React.Fragment>``` (or the short <>)

###Differences between JSX and HTML
1.  className instead of HTML's class
2.  htmlFor instead of HTML's for
3.  Every tag needs to be closed. 
        
        Examples: <img /> or <br />

4.  All event handlers and other properties need to be camelCased.
        
        Examples: onClick or onMouseOver

5.  ... List continues

---

## Common JSX + Logic

On and off (True and false toggle)

```jsx
  const [showAddFriend, setShowAddFriend] = useState(false);

  function handleShowAddFriend() {
    setShowAddFriend((show) => !show);
  }

  <Button onClick={handleShowAddFriend}>Add Friend</Button>
```
Also adding condition 

```jsx
<Button onClick={handleShowAddFriend}>
{showAddFriend ? "Close" : "Add Friend"}
```

Results:
![alt text](image-42.png)

![alt text](image-43.png)