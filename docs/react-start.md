---
## Creating a new react project

### ==Setting up a new react project==

```
npx create-react-app@5 AppName
```

```title="Existing project in a different computer"
npm install
```


!!! Note
    @ behind the create-react-app depends on what version of React you want to use.

!!! Warning
    Stay in root folder of your project.

_E:\Codes\ultimate-react-course-main\18-own\pizza-menu_

![
    In this case, pizza-menu
](image-1.png)

_After creating the project, use ```npm start``` to start the project._


## ==Manually import React library to your index.html== 

!!! Tips
    ```js
    import React from "react";
    ```

    ```
    import ReactDom from "react-dom/client";
    ```
    manually import if needed


## ==Strict Mode==

```js
const root = ReactDom.createRoot(document.getElementById("root"));
root.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>);
```
!!! Note
    _React will check if we're using outdated parts of the React API, it's a good idea to always activate it when developing React applications_

