---
## ==Async==

### ==Promises==

```js title="Example"
fetch("https://jsonplaceholder.typicode.com/todos")
  ///Once respond arrived, load the next line.
  .then((res) => res.json()) 
  ///Then we received the data from this call back function then we log to console.
  .then((data) => console.log(data));

console.log("Ricky");
```


!!! Note
    ```fetch``` function fires off a request from the API,
    
    Javascript immediately move on to next code while they are still fetching the data which is ```console.log("Ricky")```
    
    Once it finished loading the API, Javascript load the next line in the ```fetch``` function, then at last log the ```data```.
    

### ==Async, Await==

```js title="Same example but using Async, Await"
async function getTodos() {
  const res = await fetch("https://jsonplaceholder.typicode.com/todos");
  //store the awaited data into a variable
  const data = await res.json();

  console.log(data);

  return data;
}

getTodos();
console.log("Ricky")
```
!!! Note
    The waiting only works inside the Async function, so between ```getTodos();``` 
    
    and ```console.log("Ricky");```, console.log will come out first in this case.

    ```await``` will wait for the API to be fulfilled before becoming ```res```pond, the same goes for ```data```, it will wait for respond become complete .json file first.


