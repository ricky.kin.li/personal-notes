---

## Global State

-   State that **many components** might need
-   Shared state that is accessible to **every component** in the entire application

## Local State

-   State needed **only by one or few components**
-   State that is defined in a component and **Only that component and chil components** have access to it (by passing via props)


!!! Example: Search bar should be Local State, and Shopping Cart should be Global State
    ![alt text](image-32.png)


Always try to use Local State, only use Global if you truly need it.

---

## State When and Where?

![Flow Chart](image-33.png)