---
##==Setting Classes and Text Conditionally==

```jsx
function Menu() {
  const pizzas = pizzaData;
  // const pizzas = [];
  const numPizzas = pizzas.length;
  console.log(pizzas);

  return (
    <main className="menu">
      <h2>Our menu!</h2>

      {numPizzas > 0 ? (
        <>
          <p>
            Authentic Italian cuisine. 6 creative pizza dishes to choose from.
            All from our stone oven, all organic, all delicious.
          </p>
          <ul className="pizzas">
            {pizzas.map((pizza) => (
              <Pizza pizzaObj={pizza} key={pizza.name} />
            ))}
          </ul>
        </>
      ) : (
        <p>Come back later!</p>
      )}
    </main>
  );
}
```

```jsx
function Pizza({ pizzaObj }) {
  console.log({ pizzaObj });
  console.log(pizzaObj);
  console.log(pizzaObj.soldOut);

  return (
    <li className={`pizza ${pizzaObj.soldOut ? "sold-out" : ""}`}>
      <img src={pizzaObj.photoName} alt={pizzaObj.name} />
      <div>
        <h3>{pizzaObj.name}</h3>
        <p>{pizzaObj.ingredients}</p>
        <span>{pizzaObj.soldOut ? "SOLD OUT!" : pizzaObj.price}</span>
      </div>
    </li>
  );
}
```

This line ```<li className={`pizza ${pizzaObj.soldOut ? "sold-out" : ""}`}>``` - Condition: if ```pizzaObj.soldOut``` is true, it will add additional className ```sold-out```.

This line ```<span>{pizzaObj.soldOut ? "SOLD OUT!" : pizzaObj.price}</span>``` - Condition: ```if pizzaObj.soldOut``` is true, it will display ```string``` SOLD OUT!, if not display ```pizzaObj.price```.
