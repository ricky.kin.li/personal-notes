###==Rendering Lists==

###Example

```js
const pizzaData = [
  {
    name: "Focaccia",
    ingredients: "Bread with italian olive oil and rosemary",
    price: 6,
    photoName: "pizzas/focaccia.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Margherita",
    ingredients: "Tomato and mozarella",
    price: 10,
    photoName: "pizzas/margherita.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Spinaci",
    ingredients: "Tomato, mozarella, spinach, and ricotta cheese",
    price: 12,
    photoName: "pizzas/spinaci.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Funghi",
    ingredients: "Tomato, mozarella, mushrooms, and onion",
    price: 12,
    photoName: "pizzas/funghi.jpg",
    soldOut: false,
  },
];
```

```jsx
function App() {
  return (
    <div className="container">
      <Header />
      <Menu />
      <Footer />
    </div>
  );
}
```

```jsx
function Menu() {
  return (
    <main className="menu">
      <h2>Our menu!</h2>

      <ul className="pizzas">
        {pizzaData.map((pizza) => (
          <Pizza pizzaObj={pizza} key={pizza.name} />
        ))}
      </ul>
    </main>
  );
}
```

```jsx
function Pizza(props) {
  return (
    <div className="pizza">
      <img src={props.pizzaObj.photoName} alt={props.pizzaObj.name} />
      <div>
        <h3>{props.pizzaObj.name}</h3>
        <p>{props.pizzaObj.ingredients}</p>
        <span>{props.pizzaObj.price}</span>
      </div>
    </div>
  );
}
```

!!! Important
    Render **ONE** pizza element for each of the object inside the array (**pizzaData**), 
    create an new array ```pizzaObj={pizza}```, in this array there will be a new pizza component, we pass a prop 
    then we received it in ```Pizza(props)```, so using ```{props.pizzaObj.xxx}``` will take data from the mapped array.

![alt text](image-20.png)

---

You could also do it like this.

```jsx
function Menu() {
  return (
    <main className="menu">
      <h2>Our menu!</h2>

      <ul className="pizzas">
        {pizzaData.map((pizza) => (
          <li className="pizza">
             <img src={props.pizzaObj.photoName} alt={props.pizzaObj.name} />
             <div>
                <h3>{props.pizzaObj.name}</h3>
                <p>{props.pizzaObj.ingredients}</p>
                <span>{props.pizzaObj.price}</span>
             </div>
          </li>
        ))}
      </ul>
    </main>
  );
}

---
##==Conditional Rendering==

With ternaries

![alt text](image-21.png)