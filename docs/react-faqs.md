---
### Your saved your page, but the page didn't update.

_You can try below method:_

-   ```npm start```
-   ```ctrl + c``` -> ```npm start```
-   reloading your browser


### If you are in your project root, but ```npm start``` won't work

-   _Try ```npm install``` then run npm start again_