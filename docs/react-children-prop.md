---

## Children Prop Examples ##

``` jsx
<div className="buttons">
    <Button bgColor="#7950f2" textColor="#fff" onClick={handlePrevious}>
        <span>🎈</span>Previous
    </Button>
    <Button bgColor="#7950f2" textColor="#fff" onClick={handleNext}>
        Next <span>🎇</span>
    </Button>
</div>
```

``` jsx
function Button({ textColor, bgColor, onClick, children }) {
  return (
    <button
      style={{ backgroundColor: bgColor, color: textColor }}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
```

!!! Note
    The value of the children prop is exactly what is between the opening and the closing tag of the component. In this case. Between <Button> <span>🎈</span>Previous </Button> It basically fill the gaps in the {children} element.

Result:

![alt text](image-39.png)


If I changed as below.

``` jsx
<Button bgColor="#7950f2" textColor="#fff" onClick={handlePrevious}>
    Previous <span>🎈</span>
</Button>
```

Result:

![alt text](image-40.png)


---

![alt text](image-41.png)

Really useful for generic components that don't know their content before being used.