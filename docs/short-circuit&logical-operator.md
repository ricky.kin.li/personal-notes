---
## ==Logical Operator==

```title="&& Operator"
(true && "some string"); result = some string
(false && "some string"); result = false
(null && "some string"); result = null
```
```title="|| Operator"
(true || "some string"); result = "true"
(false || "some string"); result = "some string"
```
