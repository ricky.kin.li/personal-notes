---

## _HTML Element_


### Basic element 

```html
<h1>My first heading</h1>
<p>My first paragraph.</p>
<br> = line break, empty element without a closing tag.
```

### Select + Option
```html
    <select>
        <option value={1}>1</option>
        <option value={2}>2</option>
        <option value={3}>3</option>
    </select>
```

Output:

![alt text](image-28.png)

### Button
```html
<button>Add</button>
```

Output:

![alt text](image-29.png)

### Check box
```html
<input type="checkbox">
```

![alt text](image-37.png)