---

##==Props==

_Reciving Props_
Props = Property

_JSX props are a way to pass information from one component to another. Think of props as instructions or data that you can give to a React component, telling it what to do or how to behave._

```js title="As React included the pizza component (Bottom code), it basically called this function and pass in this props object"
function Menu() {
  return (
    <main className="menu">
      <h2>Our menu!</h2>
      <Pizza
        name="Pizza Spinaci"
        ingredients="Tomato, mozarella, spinach, and ricotta cheese"
        photoName="pizzas/spinaci.jpg"
        price={10}
      />

      <Pizza
        name="Pizza Funghi"
        ingredients="Tomato, mushrooms"
        price="10"
        photoName="pizzas/funghi.jpg"
      />
    </main>
  );
}

function Pizza(props) {
  return (
    <div className="pizza">
      <img src={props.photoName} alt={props.photoName} />
      <div>
        <h3>{props.name}</h3>
        <p>{props.ingredients}</p>
        <span>{props.price + 3}</span>
      </div>
    </div>
  );
}
```

_RESULT:_

![alt text](image-13.png)

!!! Note
Price for Pizza Funghi is passed in by string, in order to turn it in number, use {} to enter Javascript mode.

##==Data==

_Props / State_

![alt text](image-14.png)

- Props are read-only, they are immutable.
- Props can only be updated by the parent component.
- Mutating props would affect parent, having side effect.
  (Side-effect = changed data that is located outside of the current function.)
- Component should never mutate any data outside of it's function.
- Data should only flow from parents to children, ==_NEVER_== the opposite way.
  ![alt text](image-15.png)

!!! Benefit of one way data flow - Makes applications more predictable and easier to understand - Makes applications easier to debug, as we have more control over the data

##==Destructuring Props==

```js
function Menu() {
  const pizzas = pizzaData;
  // const pizzas = {};
  const numPizzas = pizzas.length;
  console.log(pizzas);

  return (
    <main className="menu">
      <h2>Our menu!</h2>
      {numPizzas > 0 ? (
        <ul className="pizzas">
          {pizzas.map((pizza) => (
            <Pizza pizzaObj={pizza} key={pizza.name} />
          ))}
        </ul>
      ) : (
        <p>Come back later!</p>
      )}
    </main>
  );
}
```

```jsx title="Simply add { } inside parameter"
function Pizza({ pizzaObj }) {
  console.log({ pizzaObj });
  console.log(pizzaObj);
  console.log(pizzaObj.soldOut);

  if (pizzaObj.soldOut) return null;

  return (
    <li className="pizza">
      <img src={pizzaObj.photoName} alt={pizzaObj.name} />
      <div>
        <h3>{pizzaObj.name}</h3>
        <p>{pizzaObj.ingredients}</p>
        <span>{pizzaObj.price}</span>
      </div>
    </li>
  );
}
```
