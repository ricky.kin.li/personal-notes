---
## ==Arrow Function Examples==
_Good for simple basic function_

```js title="Regular Function"
function multiplyByTwo(num) {
    return num * 2;
}
```

```js title="Arrow Function"
const multiplyByTwo = (num) => num * 2;
```

```js
let str = "ni hao";
const arrowFunc = (str) => str.toUpperCase();

result:  
str = "NI HAO"
```

---

### Functions in React ###

In first one:

```<button onClick={()=>props.submitHandler(searchInputValue)}>Submit</button>```
This is arrow function and it will trigger only onClick of the button.

In second one:

```<button onClick={props.submitHandler(searchInputValue)}>Submit</button>```
This is a normal function call , which calls the method as soon the rendering of the component happens.