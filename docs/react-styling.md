---
##==Basic Styling==

```jsx
function Header () {
    return <h1 style={{color: "red", fontSize: "50px", textTransform: "uppercase"}}>Fat Ricky Pizza Co.</h1>
}
```
_OR_

```js
function Header () {
  const style = {color: "red", fontSize: "50px", textTransform: "uppercase"};

    return <h1 style={style}>Fat Ricky Pizza Co.</h1>
}
```

!!! RESULT
    ![alt text](image-9.png)


##==Adding CSS File in your project==

![alt text](image-10.png)

![alt text](image-11.png)

Linking it at the top of your ```index.js``` file.

---

##==Loading CSS to your component==

```js title="use className instead of class in JSX"
function App() {
    return (
    <div className="container">
        <Header />
        <Menu />
        <Footer />
    </div>
    );
}
``` 

```css title="class inside the .css file"
.container {
  max-width: 80rem;
  margin: 0 auto;

  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 4.8rem;
}
```

---

!!! Examples
```js title="Header Component"
function Header () {
  // const style = {color: "red", fontSize: "50px", textTransform: "uppercase"};
  const style = {};

    return (
      <header className="header">
        <h1 style={style}>Fat Ricky Pizza Co.</h1>
      </header>
  )
}
```

```css title="CSS"
.header {
  align-self: stretch;
}

.header h1 {
  /* Non-accessible color */
  color: #edc84b;
  /* color: #af8602; */

  text-transform: uppercase;
  text-align: center;
  font-size: 5.2rem;
  font-weight: 300;
  letter-spacing: 3px;
  position: relative;
  width: 100%;
  display: block;
}

.header h1::before,
.header h1::after {
  display: block;
  content: '';
  height: 3px;
  width: 4rem;
  background-color: #edc84b;
  position: absolute;
  top: calc(50% - 1px);
}
```

!!! Result
    ![alt text](image-12.png)

---

!!! Example
    ![](image-17.png)


Result:

![alt text](image-19.png)
