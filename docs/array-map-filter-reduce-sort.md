---

_Map_, _Reduce_, _Filter_, _Sort_ () <- their parameters are all functions

## ==Array Map & Array From==

```js title="Array Map"
//Sample Array #1
let b = [1, 3, 5, 7, 9];
const changeArray = b.map((num) => num * 2);

result: changeArray = [2, 6, 10, 14, 18];
```

---

One very simple use case for this is to create an array of a specific length and fill it with an arithmetic sequence.
For example, if you want to create an array of 5 elements and fill it with the value 1 to 5, you can do it like this:
```js
const arr = Array.from({ length: 5 }, (v, i) => i + 1);
console.log(arr); // [1, 2, 3, 4, 5]
```
```jsx title="Array From #1"
  {Array.from({ length: 20 }, (_, i) => i + 1).map((num) => (
    <option value={num} key={num}>
      {num}
    </option>
  ))}
```

==More-examples==

```js title="Array From #2"
Array.from({ length: 20 }, (_, i) => i + 1);
```
is the same as creating an array with 20 slots, then mapped it with below:
```js
(_, i) => {
    return i + 1;
}
```

!!! Summary
    ```Array.from``` lets you create a new array and map it at the same time
    
    ```map``` lets you map an existing array

---


## ==Array Filter==

```js title="Sample Array #2"
let cats = [
  { id: 1, firstName: "Shibo", lastName: "Li", breed: "Dumb", age: 130 },
  { id: 2, firstName: "Daifuk", lastName: "Lam", breed: "Hk girl", age: 100 },
  {
    id: 3,
    firstName: "Someone's Cat",
    lastName: "Chan",
    breed: "Expensive",
    age: 10,
  },
];
```

```js
const filterArray = cats.filter((breed) => breed.breed.includes("Dumb"));
const filterName = cats.filter((lastN) => lastN.lastName.includes("Lam"));
const filterAge = cats.filter((age) => age.age > 110);

result: filterArray = {
  firstName: "Shibo",
  lastName: "Li",
  breed: "Dumb",
  age: 130,
};
filterName = {
  firstName: "Daifuk",
  lastName: "Lam",
  breed: "Hk girl",
  age: 100,
};
filterAge = { firstName: "Shibo", lastName: "Li", breed: "Dumb", age: 130 };
```

## ==Array Reduce (Surface Level)==

**_Using Sample Array #2 Above_**

````js
const totalAges = cats.reduce((sum, cat) => sum + cat.age, 0);
result: totalAges = 240; ///130 + 100 + 10
//retrieve two parameter, sum = accumulated value, cat = cats
//at the end of the reduce, the 0 above is the initiate number of ```sum```
````

## ==Array Sort==

```js
// Simple Array #3
const arr = [3, 7, 1, 8, 6];
const sorted = arr.sort((a, b) => a - b);

result: arr = [1, 3, 6, 7, 8];
sorted = [1, 3, 6, 7, 8];
```

```js
const sorted = arr.sort((a, b) => b - a);

result: arr = [8, 7, 6, 3, 1];
sorted = [8, 7, 6, 3, 1];
```

!!! WARNING
Above method will mutate the original array, instead use slice as sample below.

```js
const sorted = arr.slice().sort((a, b) => a - b);
result: arr = [3, 7, 1, 8, 6];
sorted = [1, 3, 6, 7, 8];
```

## ==ADDING== Object to an existing array

**_Using Sample Array #2 Above_**

```js
const newCat = {
  id: 4,
  firstName: "Melo",
  lastName: "Stupid",
  breed: "Long hair old man",
  age: 70,
};
```

You can use the SPREAD operator for adding it to the beginning.

_OR_

You can choose REST operator to add it at the end.

```js title="SPREAD Operator"
const catsAfterAddSpread = [...cats, newCat];

result:
cats =
  {id: 1,firstName: "Shibo", lastName: "Li", breed: "Dumb", age: 130},
  {id: 2,firstName: "Daifuk", lastName: "Lam", breed: "Hk girl", age: 100},
  {id: 3,firstName: "Someone's Cat", lastName: "Chan", breed: "Expensive", age: 10},
  {id: 4,firstName: "Melo",lastName: "Stupid",breed: "Long hair old man", age: 70},
```

```js title="REST Operator"
const catsAfterAddRest = [newCat, ...cats];

result:
cats =
  {id: 4,firstName: "Melo",lastName: "Stupid",breed: "Long hair old man", age: 70},
  {id: 1,firstName: "Shibo", lastName: "Li", breed: "Dumb", age: 130},
  {id: 2,firstName: "Daifuk", lastName: "Lam", breed: "Hk girl", age: 100},
  {id: 3,firstName: "Someone's Cat", lastName: "Chan", breed: "Expensive", age: 10},
```

When we want to add an new object to an array, we create an new array, `spread` the current elements, and add the new element.

## ==DELETEING== Object to an existing array

**_Using Sample Array #2 Above_**

```js title="Using catsAfterAddSpread array above"
const catsAfterDelete = catsAfterAddSpread.filter((cat) => cat.id !== 2);
// id 1 !== 2 : TRUE
// id 2 !== 2 : FALSE, id 2 === 2, so it didn't RETURN
// id 3 !== 2 : TRUE
// id 4 !== 2 : TRUE
// The function runs 4 times, due to array length is 4
result:
  {id: 1,firstName: "Shibo", lastName: "Li", breed: "Dumb", age: 130},
  // Deleted, technically filtered out.
  {id: 3,firstName: "Someone's Cat", lastName: "Chan", breed: "Expensive", age: 10},
  {id: 4,firstName: "Melo",lastName: "Stupid",breed: "Long hair old man", age: 70},
```

We use `filter()` to make the array shorter.

## ==UPDATING== Object to an existing array

```js
const catsAfterUpdate = catsAfterDelete.map((cat) => cat.id === 1 ? {...cat, age: 400, firstName: "Shitbo"} : cat);
//If cat.id equal to 1, it will update the objects :
//age: 400, firstName: "Shitbo"
//If cat.id does NOT equal to 1, it will follow original CatsAfterDelete(cat) Object
result:
  {id: 1,firstName: "Shitbo", lastName: "Li", breed: "Dumb", age: 400},
  {id: 3,firstName: "Someone's Cat", lastName: "Chan", breed: "Expensive", age: 10},
  {id: 4,firstName: "Melo",lastName: "Stupid",breed: "Long hair old man", age: 70},
```

And to update, we use the `map` method, because `map` method creates an array which has the exact same length as the original. Then when we are at the ID we are looking for, we manipulate then create a new object to override the property. For the other cats, we wimply return the cats unchanged.


