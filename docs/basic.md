---
## ==Basic Syntax==

### _Declare a variable_

```js
let str = "Strings";
let num = 12;
let func = (num) => num * 2;
let bool = true; //or false
let arr = [1, 2, 3, 4, 5];
let firstName = "John",
  lastName = "Doe",
  age = 35;
```

### _Operator_

```js
let x = 15 % 9;
result:
x equals to 6;

let y = 100 % 9;
result:
x equals to 1;
```

```js
let a = 3;
let b = 6;
//result in 9, same as x = x + y
result: a += b;
```

### _For Loop_

```js
for (let i = 0; i < 5; i++) {
  console.log("Hi");
}
```

```title="Result"
Hi
Hi
Hi
Hi
Hi
```

### _If Else Statement_

```js
if (condition) {
  executed if condition is true
} else if (condition 2) {
  executed if condition 2 is true
} else {
  if above condition 1 is false and condition 2 is false
}
```
