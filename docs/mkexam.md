# Header 1

## Header 2

Paragraph # 1

A long paragraph............[this is a link](https://www.google.com)......

_Italic_

**Bold**

**_Bold Italic_**

## List

### unordered list

- item 1
  - Subtext 1
    - Subtext 2
      - Subtext 3
      - Subtext 4
- item 2
- item 3

1. item 4
    - item 10

### ordered list

1. item 1
    - Subtext 1
2. item 2
3. item 3

### Code example

Monospace font -> `time`,

```js
let a = 10;
```

```title="Existing Project"
.\env\Scripts\activate
mkdocs serve
```

<!-- same page link -->

[this is a link](https://www.google.com)

<!-- new tab link -->

[this is a link](https://www.google.com){target=\_blank}

![this is a picture](https://ichef.bbci.co.uk/news/800/cpsprodpb/15951/production/_117310488_16.jpg.webp)
