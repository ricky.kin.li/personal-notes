---

## Building form in React

```<form>``` Element.

```jsx
function Form() {
  function handleSubmit(e) {
    e.preventDefault();
  }

  return (
    <form className="add-form" onSubmit={handleSubmit}>
      <h3> What the hell you need?</h3>
      <select>
        {Array.from({ length: 20 }, (_, i) => i + 1).map((num) => (
          <option value={num} key={num}>
            {num}
          </option>
        ))}
      </select>
      <input type="text" placeholder="Item..." />
      <button>Add</button>
    </form>
  );
}
```
Result:

![alt text](image-30.png)

!!! Important
    For single page application, please always use e.preventDefault() to prevent it from reloading onSubmit

```
function Form() {
  function handleSubmit(e) {
    e.preventDefault();
  }
```

-   This is to prevent the page from reloading.
-   the ```e``` inside handleSubmit(e) : as soon as the submit event happen, React will call this handleSubmit function, when it does, it will pass into the function, the event object ```e```.

---

## Controlled Elements

1.  Define a piece of state as below.

```js
const [description, setDescription] = useState("");
const [quantity, setQuantity] = useState(1);
```

2.  Then we use that piece of state on the element we want to control. Basically force the element to always take the value of this state variable.

```jsx
<input
  type="text"
  placeholder="Item..."
  value={description}
  onChange={(e) => setDescription(e.target.value)}
/>
```

3. Finally, we update the state variable. We do so with the `onChange` handler. Then `setDescription` to the current value of that input field.

```jsx
onChange={(e) => setDescription(e.target.value)}
```

![alt text](image-27.png)

![alt text](image-31.png)
