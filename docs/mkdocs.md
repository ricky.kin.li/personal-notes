---
## ==MK Docs Initialize==

```title="New Project"
python -m venv env
.\env\Scripts\activate
pip install -r requirments.txt
```


```title="Existing Project"
.\env\Scripts\activate
mkdocs serve
```