![alt text](image-38.png)

---

## _Optimal Combo_

### Corner Drive Impact Blocked Combo

-   DI > MP/HP(TC) > 236 HK > 262 LP > 214 HP (2302 Damage)
-   DI > HP > 236 OD.K > 262 LP > 214 LP (2492 Damage)  

### Mid Screen

-   MP/HP(TC) > 6KK/P(Non delay) > 262 MP (1920 Damage)
-   MP/HP(TC) > 214 HP (1770 Damage) *Better OKI

### Corner Meter Dump

-   HK > DR > HK > HP > DR > HK > HP > CA3 (5366 Damage)
-   JHP > HP > DR > HP > 236 OD.K > 236 LP/ 6P > HP > 214 HP > CA3 (5810 Damage)

### Big Punish

-   HP (Punish Counter) > 2 MK > 236 OD.K > 236 LP / 6P > DR > HP > 214 HP > CA3 (5412 Damage)
