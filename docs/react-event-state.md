---

##==Handling Events==

|   React Even  |   Description                |
|   :---------  |   -----------------------    |
|   [```onClick```](https://www.geeksforgeeks.org/react-onclick-event/){target=\_blank}  |   This event is used to detct mouse click in the user interface.   |
|   [```onChange```](https://www.geeksforgeeks.org/react-onchange-event/){target=\_blank}  |   This event is used to detect a change in input field in the user interface.  |
|   [```onSubmit```](https://www.geeksforgeeks.org/react-onsubmit-event/_){target=\_blank}   |       This event fires on submission of a form in the user interface and is also used to prevent the default behavior of the form.    |  
|   [```onKeyDown```](https://www.geeksforgeeks.org/react-onkeydown-event/){target=\_blank} |   This event occurs when user press any key from keyboard.    |
|   [```onKeyUp```](https://www.geeksforgeeks.org/react-onkeyup-event/){target=\_blank} |   This event occurs when user release any key from keyboard.  |
|   [```onMouseEnter```](https://www.geeksforgeeks.org/react-onmouseenter-event/){target=\_blank}   | This event occurs when mouse enters the boundary of the element.  |


```jsx title="Example"
        <button
          style={{ backgroundColor: "#7950f2", color: "#fff" }}
          // onClick={() => alert("Previous")}
          onClick={handlePrevious}
        >
          Previous
        </button>
```

---

##==State==

!!! Note
    State is a data that a **_component can hold over time_**. for information that needs to remember throughout the app's life cycle.

    Or treat it as **_Memory of a Component_**.


![alt text](image-22.png)


![alt text](image-23.png)

State can be a lot of things, but all they have in common is user can easily change these values.

---

##Use State

We can only update the state by using the tools React give us.
DO NOT Change state manually.

!!! What to do
    ```jsx
      const [step, setStep] = useState(1);

  function handlePrevious() {
    if (step > 1) setStep(step - 1);
  }```

!!! What _NOT_ to do
    ```jsx
    let [step, setStep] = useState(1);

    function handlePrevious() {
    step = step - 1
    }
    ```

---
##The Mechanic of REACT

![alt text](image-25.png)

---

##Updating state

_Example_
```jsx
function Counter() {
  const [step, setStep] = useState(0);
  const [count, setCount] = useState(0);


  return (
    <div>
      <div>
        <button onClick={() => setStep((step) => step - count)}>-</button>
        <span>Step: {step} </span>
        <button onClick={() => setStep((step) => step + count)}>+</button>
      </div>

      <div>
        <button onClick={() => setCount((count) => count - 1)}>-</button>
        <span>Count: {count} </span>
        <button onClick={() => setCount((count) => count + 1)}>+</button>
      </div>

      <div>
        <p>Today is {new Date().toJSON().slice(0, 10)}</p>
      </div>
    </div>
  );
}
```

!!! Result
    ![alt text](image-26.png)


The same three steps, first <span style="color:yellow">declare</span> the state variable, then use it in <span style="color:lightblue">JSX</span>, at last <span style="color:green">update</span> it.

<span style="color:yellow">_Declare_</span>
```const [step, setStep] = useState(0);```

<span style="color:lightblue">_Use in JSX_</span>
```<button onClick=```

<span style="color:green">_Update_</span>
```() => setStep((step) => step - count)```

---

Below:

Using ```setStep``` to change ```step```.


```jsx
const [step, setStep] = useState(0);


onChange={(e) => setStep(Number(e.target.value))}
```


