---
## ==Ternary Operator==

```js
let pages = 1190;
const pagesAbove = pages > 1000 ? "It's over a thousand" : "Less than 1000"
// 1190 is over 1000, therefore it is true.
result:

pagesAbove = "It's over a thousand"
```

```js
let shiboIsACat = false;
const really = shiboIsACat ? "Yes he is!" : "He is not.";
result:

shiboIsACat = "He is not."
```

---

!!! Examples
```jsx
function Footer() {
  const hour = new Date().getHours();
  const openHour = 10;
  const closeHour = 22;
  const isOpen = hour > openHour && hour < closeHour;

  return (
    <footer className="footer">
      {/* Javascript Mode */}
      {isOpen ? (
        <div className="order">
          <p>We're open until {closeHour}:00! Come visit us or order online.</p>
          <button className="btn">Order</button>
        </div>
      ) : (
        <p>
          We close mother fucker! Come back in {openHour}:00 in the morning.{" "}
        </p>
      )}
    </footer>
  );
}
```

If ```isOpen``` equals **true**, return 
```jsx
(
<div className="order">
    <p>We're open until {closeHour}:00! Come visit us or order online.</p>
    <button className="btn">Order</button>
</div>
)
```

Else
```jsx
(
<p>We close mother fucker! Come back in {openHour}:00 in the morning.</p>
)
```