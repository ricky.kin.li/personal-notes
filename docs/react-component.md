---
##==Building blocks of React==

Everything is build off components in React.

Each component has its ==own data==, ==Javascript logic==, and ==appearance==.
![alt text](image-2.png)

Building complex UI and combine them like Lego pieces.
![alt text](image-3.png)

Nesting component inside each other.
![alt text](image-4.png)

Create one question component, and reuse them three times.

Now, of course, the data for each of them is different,

but we can easily pass different data

into different question components

by using something called props.

![](image-5.png)

##==Component Trees & Relationship==

It's best to create a tree like this to clearly see relationship between component.

![alt text](image-6.png)
