##==Snippet==

_My Own Snippet_ 

```js
{
	"Print to console": {
	  "prefix": "cl",
	  "scope": "javascript,typescript,javascriptreact",
	  "body": ["console.log($1)"],
	  "description": "console.log"
	},
	"reactComponent": {
	  "prefix": "rfc",
	  "scope": "javascript,typescript,javascriptreact",
	  "body": [
		"function ${1:${TM_FILENAME_BASE}}() {",
		"\treturn (",
		"\t\t<div>",
		"\t\t\t$0",
		"\t\t</div>",
		"\t)",
		"}",
		"",
		"export default ${1:${TM_FILENAME_BASE}}",
		""
	  ],
	  "description": "React component"
	},
	"reactStyledComponent": {
	  "prefix": "rsc",
	  "scope": "javascript,typescript,javascriptreact",
	  "body": [
		"import styled from 'styled-components'",
		"",
		"const Styled${TM_FILENAME_BASE} = styled.$0``",
		"",
		"function ${TM_FILENAME_BASE}() {",
		"\treturn (",
		"\t\t<Styled${TM_FILENAME_BASE}>",
		"\t\t\t${TM_FILENAME_BASE}",
		"\t\t</Styled${TM_FILENAME_BASE}>",
		"\t)",
		"}",
		"",
		"export default ${TM_FILENAME_BASE}",
		""
	  ],
	  "description": "React styled component"
	}
  }
```

##==Extensions==

1.  Eslint
2.  Prettier (Buggy with markdown files)
3.  Quokka.js
4.  Monokai Pro (Theme)
5.	vscode-pets (ctrl + p) ```start coding with pet```
6.	Power Mode (Settings)

